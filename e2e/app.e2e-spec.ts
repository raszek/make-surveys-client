import { MakeSurveysClientPage } from './app.po';

describe('make-surveys-client App', () => {
  let page: MakeSurveysClientPage;

  beforeEach(() => {
    page = new MakeSurveysClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
