import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { UserService } from './services/user.service';
import { SurveyService } from './services/survey.service';
import { SurveyResolver } from './services/survey-resolver.service';

import { MainpageComponent } from './pages/mainpage/mainpage.component';
import { LoginComponent } from './pages/login/login.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { SurveyGeneratorComponent } from './pages/survey-generator/survey-generator.component';
import { FillSurveyComponent } from './pages/fill-survey/fill-survey.component';
import { SurveyResultsComponent } from './pages/survey-results/survey-results.component';

const routes: Routes = [
    { path: '', redirectTo: '/mainpage', pathMatch: 'full' },
    { path: 'mainpage', component: MainpageComponent },
    { path: 'login', component: LoginComponent },
    { path: 'survey-results/:id', component: SurveyResultsComponent },
    { path: 'welcome', component: WelcomeComponent, canActivate: [AuthGuard] },
    { path: 'survey-generator', component: SurveyGeneratorComponent, canActivate: [AuthGuard] },
    { path: 'fill-survey/:id', component: FillSurveyComponent, canActivate: [AuthGuard], resolve: [SurveyResolver] }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [
        AuthGuard,
        UserService,
        SurveyService,
        SurveyResolver
    ]
})
export class AppRoutingModule { }
