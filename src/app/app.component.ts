import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  email: string;

  ngOnInit() {
    this.updateNavbar();
  }

  updateNavbar(){
    this.email = null;

    let user = JSON.parse(localStorage.getItem('currentUser'));
    if (user && user.email) {
      this.email = user.email;
    }
  }

}
