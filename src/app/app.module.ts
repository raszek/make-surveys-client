import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { DndModule } from 'ng2-dnd';
import { CollapseModule } from 'ng2-bootstrap/collapse';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainpageComponent } from './pages/mainpage/mainpage.component';
import { LoginComponent } from './pages/login/login.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { SurveyGeneratorComponent } from './pages/survey-generator/survey-generator.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { SurveyFormComponent } from './components/survey-form/survey-form.component';
import { QuestionComponent } from './components/question/question.component';
import { SurveyListComponent } from './components/survey-list/survey-list.component';
import { CutPipe } from './pipes/cut.pipe';
import { FillSurveyComponent } from './pages/fill-survey/fill-survey.component';
import { SurveyResultsComponent } from './pages/survey-results/survey-results.component';


@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    LoginComponent,
    LoginFormComponent,
    SurveyGeneratorComponent,
    WelcomeComponent,
    SurveyFormComponent,
    QuestionComponent,
    SurveyListComponent,
    CutPipe,
    FillSurveyComponent,
    SurveyResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    DndModule.forRoot(),
    CollapseModule.forRoot(),
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
