import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { AppComponent } from '@app/app.component';
import { LoginForm } from '@app/models/login-form';
import { UserService } from '@app/services/user.service';

@Component({
  selector: 'app-login-form',
  providers: [UserService],
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  model = new LoginForm();

  errorMessage: string;

  formErrors = {
    'email': '',
    'password': ''
  };

  validationMessages = {
    'email': {
      'required': 'Email is required.',
      'maxlength': 'Email cannot be more than 30 characters long.',
      'minlength': 'Email cannot be less than 3 characters long.'
    },
    'password':{
      'required': 'Password is required',
      'maxlength': 'Email cannot be more than 30 characters long.'
    }
  };

  @ViewChild('loginForm') currentForm: NgForm;

  ngAfterViewChecked() {
    this.formChanged();
  }

  formChanged() {

    if (this.currentForm) {
      this.currentForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }

  onValueChanged(data?: any) {
    
    if (!this.currentForm) { return; }
    const form = this.currentForm.form;
    
    for (const field in this.formErrors) {
      
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];

        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {

    this.userService.login(this.model).subscribe(
      user => {
        this.appComponent.updateNavbar();
        this.router.navigate(['/welcome']);
      },
      error => this.errorMessage = error
    );

  }

  constructor(private userService: UserService, private appComponent: AppComponent, public router: Router) {

  }

  ngOnInit() {
    this.userService.logout();
    this.appComponent.updateNavbar();
  }

}
