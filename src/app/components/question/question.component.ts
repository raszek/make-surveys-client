import { Component, OnInit, Input } from '@angular/core';

import { Question } from '@app/models/question';
import { Answer } from '@app/models/answer';

import { SurveyFormComponent } from '@app/components/survey-form/survey-form.component';

@Component({
  selector: 'question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  maxAnswers = 8;
  minAnswers = 2;

  errorMessage: string;

  @Input() question: Question;
  @Input() index: number;

  constructor(private surveyFormComponent: SurveyFormComponent) { }

  removeQuestion() {
    this.surveyFormComponent.removeQuestion(this.index);
  }

  addAnswer() {
    if (this.question.answers.length < this.maxAnswers) {
      this.errorMessage = null;
      this.question.answers.push(new Answer);  
    }else{
      this.errorMessage = "Too many answers";
    }
    
  }

  removeAnswer(index: number) {
    if (this.question.answers.length > this.minAnswers) {
      this.errorMessage = null;
      this.question.answers.splice(index, 1);  
    }else{
      this.errorMessage = "It must be atleast " + this.minAnswers + " answer(s)."
    }
    
  }

  ngOnInit() {
  }

}
