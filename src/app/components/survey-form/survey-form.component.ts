import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Survey } from '@app/models/survey';
import { Question } from '@app/models/question';
import { Answer } from '@app/models/answer';

import { SurveyService } from '@app/services/survey.service';

import { QuestionComponent } from '@app/components/question/question.component';

@Component({
  selector: 'app-survey-form',
  templateUrl: './survey-form.component.html',
  styleUrls: ['./survey-form.component.css']
})
export class SurveyFormComponent implements OnInit {

  errorMessage: string;

  maxQuestions: number = 7;
  minQuestions: number = 1;

  model = new Survey();

  constructor(private surveyService: SurveyService, private router: Router) { }

  onSubmit() {
    this.surveyService.create(this.model).subscribe(
      data => {
        if (data.result == 'success') {
          this.router.navigate(['/welcome']);
        } else {
          this.errorMessage = "Server validation error";
        }
      }
    );
  }

  addQuestion() {
    if (this.model.questions.length < this.maxQuestions) {
      this.errorMessage = null;
      this.model.questions.push(new Question);  
    }else{
      this.errorMessage = "Too many questions.";
    }
    
  }

  removeQuestion(index: number) {
    this.model.questions.splice(index, 1);
  }

  ngOnInit() {

  }

}
