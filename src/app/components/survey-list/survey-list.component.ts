import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Survey } from '@app/models/survey';

import { SurveyService } from '@app/services/survey.service';

import { CutPipe } from '@app/pipes/cut.pipe';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {

  constructor(private surveyService: SurveyService, private router: Router) { }

  searchTerm: string;
  surveys: Survey[];
  surveysCopy: Survey[];

  ngOnInit() {
    let surveys = this.surveyService.getAll().subscribe(
      surveys => {
        console.log(surveys);
        this.surveys = surveys;
        this.surveysCopy = surveys;
      }
    );

  }

  onSelect(survey: Survey) {
    this.router.navigate(['/fill-survey', survey.id]);
  }

  search(): void {
    let term = this.searchTerm;
    this.surveys = this.surveysCopy.filter(function (tag) {
      return tag.name.indexOf(term) >= 0;
    });
  }

}
