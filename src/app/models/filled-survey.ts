import { FilledAnswer } from '@app/models/filled-answer';

export class FilledSurvey {

    surveyId: number;

    filledAnswers: Array<FilledAnswer> = [];

}
