import { Answer } from '@app/models/answer';

export class Question {

    id: number;
    question: string;

    answers: Answer[] = [new Answer, new Answer];

}
