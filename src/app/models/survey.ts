import { Question } from '@app/models/question';

export class Survey {

    id: number;
    name: string;
    description: string;
    created_by: number;

    questions: Question[] = [new Question];

}
