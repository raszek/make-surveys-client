import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { Survey } from '@app/models/survey';
import { FilledSurvey } from '@app/models/filled-survey';
import { FilledAnswer } from '@app/models/filled-answer';

import { SurveyService } from '@app/services/survey.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-fill-survey',
  templateUrl: './fill-survey.component.html',
  styleUrls: ['./fill-survey.component.css']
})
export class FillSurveyComponent implements OnInit {

  survey: Survey = new Survey;

  filledSurvey: FilledSurvey = new FilledSurvey;

  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private surveyService: SurveyService
  ) { }

  onSubmit() {
    this.surveyService.fillSurvey(this.filledSurvey).subscribe(success => {
      console.log(success);
      if (success) {
        this.router.navigate(['/welcome']);
      } else {
        this.errorMessage = 'Something wrong happened';
      }
    });
  }

  ngOnInit() {
    this.route.data
      .subscribe((data) => {
        
        this.survey = data[0].survey;
        this.filledSurvey = data[0].filledSurvey;
      });

  }

}
