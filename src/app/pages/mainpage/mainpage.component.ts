import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SurveyService } from '@app/services/survey.service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {

  pieChartType: string = 'pie';

  surveys = [];

  constructor(private surveyService: SurveyService, private router: Router) { }

  ngOnInit() {
    this.surveyService.getSurveysOverview().subscribe( data => {
      this.surveys = data;
    });
  }

  checkSurveyResults(id: number) {
    this.router.navigate(['/survey-results', id]);
  }

}
