import { Component, OnInit } from '@angular/core';

import { SurveyFormComponent } from '@app/components/survey-form/survey-form.component';

@Component({
  selector: 'app-survey-generator',
  templateUrl: './survey-generator.component.html',
  styleUrls: ['./survey-generator.component.css']
})
export class SurveyGeneratorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
