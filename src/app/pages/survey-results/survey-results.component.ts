import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SurveyService } from '@app/services/survey.service';

@Component({
  selector: 'app-survey-results',
  templateUrl: './survey-results.component.html',
  styleUrls: ['./survey-results.component.css']
})
export class SurveyResultsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private surveyService: SurveyService) { }

  chartType: string = 'pie';
  surveyResults = [];

  ngOnInit() {
    this.route.params
    // (+) converts string 'id' to a number
    .switchMap((params: Params) => this.surveyService.getSurveyResults(+params['id']))
    .subscribe((result) => {
      this.surveyResults = result;
      
    });
  }

  goBack() {
    this.router.navigate(['/mainpage']);
  }

}
