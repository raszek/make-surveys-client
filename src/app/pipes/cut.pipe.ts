import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cut'
})
export class CutPipe implements PipeTransform {

  transform(value: string, wordsNumber: number): string {
    let words = value.split(' ');

    if (words.length < wordsNumber) {
      return value;
    }

    return words.splice(0, wordsNumber).join(' ') + '(...)';
  }

}
