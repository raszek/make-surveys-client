import { TestBed, inject } from '@angular/core/testing';

import { SurveyResolveService } from './survey-resolve.service';

describe('SurveyResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SurveyResolveService]
    });
  });

  it('should ...', inject([SurveyResolveService], (service: SurveyResolveService) => {
    expect(service).toBeTruthy();
  }));
});
