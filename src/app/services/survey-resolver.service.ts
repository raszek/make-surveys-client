import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot,
         ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { SurveyService } from '@app/services/survey.service';
import { Survey } from '@app/models/survey';
import { FilledSurvey } from '@app/models/filled-survey';
import { FilledAnswer } from '@app/models/filled-answer';



@Injectable()
export class SurveyResolver implements Resolve<any> {

  constructor(private serveyService: SurveyService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let id = route.params['id'];

    return this.serveyService.getSurvey(id).map(survey => {
      if (survey) {
        let filledSurvey = new FilledSurvey;
        filledSurvey.surveyId = id;
        for (let question of survey.questions) {
          let filledAnswer = new FilledAnswer;
          filledAnswer.questionId = question.id;
          filledAnswer.answerId = question.answers[0].id;
          filledSurvey.filledAnswers.push(filledAnswer);
        }
        return {survey, filledSurvey};
      } else { // id not found
        this.router.navigate(['/welcome']);
        return null;
      }
    });

  }

}
