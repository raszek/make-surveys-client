import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import 'rxjs/add/observable/of';

import { AppConfig } from '@app/app.config';
import { Survey } from '@app/models/survey';
import { FilledSurvey } from '@app/models/filled-survey';

@Injectable()
export class SurveyService {

  currentUser = JSON.parse(localStorage.getItem('currentUser')); 

  constructor(private http: Http) { }

  getSurveysOverview() {

    return this.http.get(AppConfig.API_ENDPOINT+'/surveys-overview')
      .map((response: Response) => response.json())
  }

  getSurveyResults(id: number) {
    
    return this.http.get(AppConfig.API_ENDPOINT+'/survey-results/'+id)
      .map((response: Response) => response.json())
  }

  getAll() {

    return this.http.get(AppConfig.API_ENDPOINT+'/surveys', this.jwt())
      .map((response: Response) => response.json());
  }

  getSurvey(id: number) {

    return this.http.get(AppConfig.API_ENDPOINT+'/surveys/'+id, this.jwt())
      .map((response: Response) => {
        
        return response.json();
      });
  }

  create(Survey: Survey) { 
    
    return this.http.post(AppConfig.API_ENDPOINT+'/surveys', Survey, this.jwt())
                    .map((response: Response) => response.json());
  }

  fillSurvey(answers: FilledSurvey) {

    return this.http.post(AppConfig.API_ENDPOINT+'/surveys/fill-survey', answers, this.jwt())
                    .map((response: Response) => response.json());
  }

  private jwt() {
    
    if (this.currentUser && this.currentUser.JWT) {
      let headers = new Headers({'Authorization': 'Bearer ' + this.currentUser.JWT });
      
      return new RequestOptions({ headers: headers });
    }
  }

}
