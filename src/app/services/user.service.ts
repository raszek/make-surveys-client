import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AppConfig } from '@app/app.config';
import { User } from '@app/models/user';
import { LoginForm } from '@app/models/login-form';

@Injectable()
export class UserService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private loginUrl = AppConfig.API_ENDPOINT+'/login';

  public redirectUrl: string;

  constructor(private http: Http) { }

  login(loginForm: LoginForm): Observable<User> {

    var jsonData = {
      User: {
        email: loginForm.email,
        password_hash: loginForm.password
      }
    }

    return this.http
      .post(this.loginUrl, jsonData, { headers: this.headers })
      .map(this.extractData)
      .catch(this.handleError);
  }

  logout(): void {
    localStorage.removeItem('currentUser');
  }

  private extractData(res: Response) {

    let body = res.json();

    if (body.email && body.JWT){
      localStorage.setItem('currentUser', JSON.stringify(body));
    }

    return body;
  }

  private handleError(error: Response | any) {


    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    return Observable.throw(errMsg);
  }

}
